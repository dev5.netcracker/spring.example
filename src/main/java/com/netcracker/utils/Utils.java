package com.netcracker.utils;

import com.netcracker.interfaces.Car;

import java.util.Scanner;
import java.util.StringJoiner;

public class Utils {

    private Utils() {
    }

    public static final String NEW_LINE = "\r\n";
    public static final String DELIMITED = " ";

    public static int getInput() {
        int id = -1;
        Scanner in = new Scanner(System.in);

        if (in.hasNextInt()) {
            id = in.nextInt();
        }

        return id;
    }

    public static String getInfo(Car car){
        StringJoiner stringJoiner = new StringJoiner(DELIMITED, NEW_LINE, NEW_LINE);
        stringJoiner.add(car.getName());
        stringJoiner.add(String.valueOf(car.hashCode()));
        return stringJoiner.toString();
    }
}
