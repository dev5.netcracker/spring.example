package com.netcracker;

import com.google.common.collect.Streams;
import com.netcracker.interfaces.Car;
import com.netcracker.utils.Utils;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class Console {

  private final List<Car> cars;
  private final Car superCar;

  @Autowired
  public Console(List<Car> cars, Car superCar) {
    this.cars = cars;
    this.superCar = superCar;
  }

  public void start() {

    while (true) {
      Streams.mapWithIndex(cars.stream(), ((car, index) -> index + ":" + car.getName()))
          .forEach(System.out::println);

      int id = Utils.getInput();

      if (id == -2) {
        print(superCar);
        continue;
      }

      if (id < 0 || id >= cars.size()) {
        System.err.println("Error");
        continue;
      }

      Car car = cars.get(id);

      print(car);
    }
  }

  private void print(Car car) {
    String info = Utils.getInfo(car);
    System.out.println(info);
  }


}
