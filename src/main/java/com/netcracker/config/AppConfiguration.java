package com.netcracker.config;

import com.netcracker.impl.Mercedes;
import com.netcracker.interfaces.Car;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfiguration {

    @Bean
    public Car mercedes() {
        return new Mercedes();
    }
}
