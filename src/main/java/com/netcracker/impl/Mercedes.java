package com.netcracker.impl;

import com.netcracker.interfaces.Car;

public class Mercedes implements Car {

  @Override
  public String getName() {
    return "Mercedes";
  }
}
