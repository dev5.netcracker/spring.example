package com.netcracker.impl;

import com.netcracker.interfaces.Car;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component("mazda2")
@Primary
public class Mazda implements Car {

    @Override
    public String getName() {
        return "Mazda";
    }
}
