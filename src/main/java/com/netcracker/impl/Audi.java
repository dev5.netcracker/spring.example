package com.netcracker.impl;

import com.netcracker.interfaces.Car;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Scope("prototype")
@Component
public class Audi implements Car {
    @Override
    public String getName() {
        return "Audi";
    }
}
