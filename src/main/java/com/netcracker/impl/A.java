package com.netcracker.impl;

import com.netcracker.interfaces.Car;
import org.springframework.stereotype.Component;

//@Component
public class A implements Car {
    @Override
    public String getName() {
        return "A";
    }
}
